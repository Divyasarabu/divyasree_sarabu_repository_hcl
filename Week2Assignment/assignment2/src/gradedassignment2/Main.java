package gradedassignment2;
import java.util.ArrayList;


public class Main {
	public static void main(String[] args) {

		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		Employee e4 = new Employee();
		Employee e5 = new Employee();
		
		System.out.println("List of employees");

		// setting details of employee1	
		e1.setId(1);
		e1.setName("Aman ");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");

		// setting details of employee2	
		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("Hr");
		e2.setCity("Bombay");

		// setting details of employee3	
		e3.setId(3);
		e3.setName("Zoe ");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin ");
		e3.setCity("Delhi");

		// setting details of employee4	
		e4.setId(4);
		e4.setName("Smitha ");
		e4.setAge(21);
		e4.setSalary(1000000 );
		e4.setDepartment("IT");
		e4.setCity("Chennai");

		// setting details of employee5	
		e5.setId(5);
		e5.setName("Smitha ");
		e5.setAge(24 );
		e5.setSalary(12000000);
		e5.setDepartment("Hr");
		e5.setCity("Bengaluru");

		//show employee details 

		e1.EmployeDetails();
		e2.EmployeDetails();
		e3.EmployeDetails();
		e4.EmployeDetails();
		e5.EmployeDetails();
		
		
		System.out.println();
		System.out.println("Names of all employees in the sorted order are:");

		ArrayList<Employee> emp = new ArrayList<Employee>();
		emp.add(e1);
		emp.add(e2);
		emp.add(e3);
		emp.add(e4);
		emp.add(e5);
		
		

		EmployeeNameSort empname = new EmployeeNameSort();
		empname.sortingNames(emp);

		System.out.println();
		System.out.println("Count of Employees from each city:");


		City cityname = new City();
		cityname.CityCount(emp);

		System.out.println();
		System.out.println("Monthly Salary of employee along with their ID is:");

		EmpSalary empsalary = new EmpSalary();
		empsalary.salary(emp);

	}

}
