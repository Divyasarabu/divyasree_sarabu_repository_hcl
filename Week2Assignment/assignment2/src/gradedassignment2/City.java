package gradedassignment2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeMap;

public class City {
	public void CityCount(ArrayList<Employee> employees) {
		ArrayList<String> city = new ArrayList<>();
		city.add(employees.get(0).getCity());
		city.add(employees.get(1).getCity());
		city.add(employees.get(2).getCity());
		city.add(employees.get(3).getCity());
		city.add(employees.get(4).getCity());
		
		
		HashSet<String> citycountname = new HashSet<String>(city);
		TreeMap<String,Integer> tm=new  TreeMap<String,Integer> ();  
		for(String cityname2: citycountname) {
			tm.put(cityname2,Collections.frequency(city, cityname2) );  
		}
		System.out.println( tm);
	}
	}
