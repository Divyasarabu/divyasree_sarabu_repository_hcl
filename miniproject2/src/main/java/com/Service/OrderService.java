package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.UserOrder;
import com.dao.OrderDao;

@Service
public class OrderService {
	
	@Autowired
	OrderDao orderDao;
    
    public String storeOrder(UserOrder order) {
    int sno=(int)orderDao.count();
    System.out.println(sno);
    order.setSno(sno+1);
    orderDao.save(order);
    return "orderStored sucessfully";
    }
    
    public List<UserOrder> getAllItemsOrdered(String name){
    	return orderDao.getItemsOrderedByName(name);
    }
    public float getTotalBill(String name) {
    	return orderDao.getTotalByName(name);
    }
    
    public List<UserOrder> getAllBillsGeneratedToday(){
    	return orderDao.findAll();
    }
    
    public float getTodaybill() {
    	return orderDao.getTodayEarnings();
    }
}
