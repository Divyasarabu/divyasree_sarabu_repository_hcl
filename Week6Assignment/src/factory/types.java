package factory;

public class types {

	public static Classification setmoviedetailsType(int type) {

		if(type==1) {
			return new Upcoming();
		}
		else if(type==2) {
			return new  releasedmovies();
		}
		else if(type==3) {
			return new bestmovieoftheyear();
		}
		else if(type==4) {
			return new topratedmovie();
		}
		else {
			return null;
		}
	}
}
