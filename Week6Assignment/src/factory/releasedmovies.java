package factory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import week6.DataBaseConnection;
import movie.moviedetails;
public class releasedmovies  implements Classification{
	// Connection Establishing
		Connection con =DataBaseConnection.getInstance().getConnection();
		public releasedmovies() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public List<moviedetails> moviedetailsType() throws SQLException {

			//To display movies from database
			List<moviedetails> movies=new ArrayList<moviedetails>();
			String sql="select id,title,year,storyline,imdbRating,classification from moviedataset where Classification ='movies in theaters'";
			Statement statement =con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next())
			{

				moviedetails movie = new moviedetails();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setStoryline(rs.getString(4));
				movie.setImdbRating(rs.getInt(5));
				movie.setClassification(rs.getString(6));
				movies.add(movie);
			}

			return movies;	
		}

	}
