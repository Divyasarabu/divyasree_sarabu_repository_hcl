package factory;
import java.sql.SQLException;
import java.util.List;

import movie.moviedetails;


public interface Classification {

	public List<moviedetails> moviedetailsType() throws SQLException;

}

